import React, { Component  } from 'react'

import {findCaregiverById} from "../../service/ApiService";
import "./MyCaregiverAccount.css"
import AuthenticationService from "../../service/AuthenticationService";
import pro from './care.png';

const INSTRUCTOR = AuthenticationService.getLoggedInUserName();

function getValue(key){
    return localStorage.getItem(key);
}

class MyCaregiverAccount extends Component{

    constructor(props){
        super(props);
        this.state ={
            id: '',
            name: '',
            birthdate: null,
            gender: '',
            address: '',
            medicalRecord:[],
            toCaregiver:false
        }
        this.getCaregiver = this.getCaregiver.bind(this);
    }

    componentDidMount() {
        this.getCaregiver();
    }

    getCaregiver =()=> {
        const id = getValue("viewcareId");
        findCaregiverById(INSTRUCTOR,id).then((caregiver) => {
            this.setState({
                id: caregiver.data.id,
                name: caregiver.data.name,
                birthdate: caregiver.data.birthdate,
                gender: caregiver.data.gender,
                address: caregiver.data.address,
            })
        });
    }

    cancel=(e)=>{this.setState(() => ({toCaregiver: true}));}

    render(){
        if (this.state.toCaregiver === true) {
            this.props.history.push('/caregiver/${INSTRUCTOR}/all')
        }
        return(
            <form >
                <div className="form-style-2">
                    <div className="form-style-2-heading"> Caregiver  view [{this.state.id}] details</div>
                    <img src={pro} alt="logo"/>

                    <div>
                        <label> *Name:  {this.state.name} </label>
                    </div>

                    <div>
                        <label> *Birthdate: {this.state.birthdate} </label>
                    </div>

                    <div>
                        <label> *Gender: {this.state.gender} </label>
                    </div>

                    <div>
                        <label > *Address: {this.state.address} </label>
                    </div>

                    <br/>
                    <br/>
                    <button className="pad"  onClick={this.cancel}> Cancel </button>
                </div>
            </form>
        )
    }
}

export default MyCaregiverAccount