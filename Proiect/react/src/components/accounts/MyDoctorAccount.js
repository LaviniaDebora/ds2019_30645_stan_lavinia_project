import React, { Component  } from 'react'
import './MyDoctorAccount.css'
import pro from './doc.png';
import AuthenticationService from "../../service/AuthenticationService";

const INSTRUCTOR = AuthenticationService.getLoggedInUserName();

class MyDoctorAccount extends Component{


    constructor(props){
        super(props);
        this.state ={
            toPatients:false,
            toCaregivers:false,
            toMedications:false
        }
    }

    managePatients=(e)=>{
        this.setState(() => ({toPatients: true}));
    }

    manageCaregivers=(e)=>{
        this.setState(() => ({toCaregivers: true}));
    }

    manageMedications=(e)=>{
        this.setState(() => ({toMedications: true}));
    }

   render(){

       if (this.state.toPatients === true) {
           console.log(INSTRUCTOR)
           this.props.history.push('/patient/${INSTRUCTOR}/all')
       }

       if (this.state.toCaregivers === true) {
           this.props.history.push('/caregiver/${INSTRUCTOR}/all')

       }

       if (this.state.toMedications === true) {
           this.props.history.push('/medication/${INSTRUCTOR}/all')

       }

        return(
            <div>
            <div className="Account">
                <h2 className="header">My Doctor Account</h2>
                <img src={pro} alt="logo"/>
            </div>
                      <div className="pad-top">
                        <button className="myBlueButton" onClick={this.managePatients}>Manage Patients</button>
                      </div>

                      <div className="pad-top">
                        <button className="myBlueButton" onClick={this.manageCaregivers}>Manage Caregivers</button>
                      </div>

                      <div className="pad-top">
                        <button className="myBlueButton" onClick={this.manageMedications}>Manage Medications</button>
                      </div>
            </div>)
      }

}

export default MyDoctorAccount