import React, { Component  } from 'react'
import {findPatientById} from "../../service/ApiService";
import "./MyPatientAccount.css"
import AuthenticationService from "../../service/AuthenticationService";
import pro from './pro.png';

const INSTRUCTOR = AuthenticationService.getLoggedInUserName();

function getValue(key){
    return localStorage.getItem(key);
}


class MyPatientAccount extends Component{

    constructor(props){
        super(props);
        this.state ={
            id: '',
            name: '',
            birthdate: null,
            gender: '',
            address: '',
            medicalRecord:[],
            toPatient:false
        }
        this.getPatient = this.getPatient.bind(this);
    }

    componentDidMount() {
        this.getPatient();
    }

    getPatient =()=> {
        const id = getValue("viewId");
        findPatientById(INSTRUCTOR,id).then((patient) => {
            this.setState({
                id: patient.data.id,
                name: patient.data.name,
                birthdate: patient.data.birthdate,
                gender: patient.data.gender,
                address: patient.data.address,
                medicalRecord: patient.data.medicalRecord,
            })
        });
    }

    cancel=(e)=>{this.setState(() => ({toPatient: true}));}

    render(){
        if (this.state.toPatient === true) {
            this.props.history.push('/patient/${INSTRUCTOR}/all')
        }
        return(
            <form >
                <div className="form-style-2">
                    <div className="form-style-2-heading"> Patient  view [{this.state.id}] details</div>
                    <img src={pro} alt="logo"/>

                    <div>
                        <label> *Name:  {this.state.name} </label>
                    </div>

                    <div>
                        <label> *Birthdate: {this.state.birthdate} </label>
                    </div>

                    <div>
                        <label> *Gender: {this.state.gender} </label>
                    </div>

                    <div>
                        <label > *Address: {this.state.address} </label>
                    </div>

                    <div>
                        <label> *Medical Record:  {this.state.medicalRecord}</label>
                    </div>

                    <br/>
                    <br/>
                    <button className="pad"  onClick={this.cancel}> Cancel </button>
                </div>
            </form>
        )
    }
}

export default MyPatientAccount