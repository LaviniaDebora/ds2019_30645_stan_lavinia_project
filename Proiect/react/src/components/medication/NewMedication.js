import React, { Component } from 'react'
import "./NewMedication.css";
import {addNewMedication} from "../../service/ApiService";
import AuthenticationService from "../../service/AuthenticationService";

const INSTRUCTOR = AuthenticationService.getLoggedInUserName();

class NewMedication extends Component {

    constructor(props){
        super(props);
        this.state ={
            id: '',
            name: '',
            dosage: null,
            toMedication:false,
            toMedicationSave:false
        }
        this.addMedication = this.addMedication.bind(this);
    }

    myChangeHandler = (e) =>
        this.setState({ [e.target.name]: e.target.value });

    addMedication = (e) => {
        e.preventDefault();
        const{id, name, dosage}=this.state;
        let medication = {
            id: id,
            name: name,
            dosage: dosage};

        addNewMedication(INSTRUCTOR, medication)
            .then(response => {
                this.setState(() => ({toMedicationSave: true}));
            });
    }

    cancelMedication=(e)=>{
        this.setState(() => ({toMedication: true}));
    }

    render() {

        if (this.state.toMedication === true) {
            this.props.history.push('/medication/${INSTRUCTOR}/all')
        }
        if (this.state.toMedicationSave === true) {
            this.props.history.push('/medication/${INSTRUCTOR}/all')
        }
        return (
            <form >
                <div className="form-style-2">
                    <div className="form-style-2-heading"> New medication registration:</div>

                    <div>
                        <label>Name:  </label>
                        <input
                            className="input-field"
                            type='text'
                            name='name'
                            onChange={this.myChangeHandler}
                        />
                    </div>

                    <div>
                        <label>Dosage: </label>
                        <input
                            className="input-field"
                            type='number'
                            name='dosage'
                            onChange={this.myChangeHandler}
                        />
                    </div>
                    <br/>
                    <br/>

                    <button  className="myGreenButton" onClick={this.addMedication}> Create </button>
                    <button className="pad"  onClick={this.cancelMedication}> Cancel </button>
                </div>
            </form>
        );
    }
}

export default NewMedication;