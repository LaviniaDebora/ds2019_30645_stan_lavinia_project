import React, { Component } from 'react'
import './Medication.css';
import  {getAllMedications,deleteMedication} from "../../service/ApiService";
import AuthenticationService from "../../service/AuthenticationService";

function storeValue(key,value){
    localStorage.setItem(key, value);
}

function removeValue(key){
    localStorage.removeItem(key);
}

const INSTRUCTOR = AuthenticationService.getLoggedInUserName();

class Medication extends Component {
    constructor(props) {
        super(props);
        this.state = {
            toUpdate: false,
            toCreate: false,
            storeId: true,
            medications: [],
            toAccount: false
        };
        this.newMedication=this.newMedication.bind(this);
        this.updateMedication=this.updateMedication.bind(this);
        this.listMedications = this.listMedications.bind(this);
        this.deleteMedication=this.deleteMedication.bind(this);
    }

    componentDidMount() {
        this.listMedications();
    }

    newMedication = () => {
        const {storeId}=this.state;
        removeValue("medicationId");
        this.setState(
            () => (
                {toCreate: true}
            ));
    }

    updateMedication = (id) => {
        const {storeId}=this.state;
        storeValue("medicationId", storeId ? id :" ");
        this.setState(
            () => (
                {toUpdate: true}
            ));
    }

    listMedications = () => {
        getAllMedications(INSTRUCTOR).
        then(
            response => {
                this.setState(
                    {
                        medications: response.data
                    }
                )
            })
    }

    deleteMedication= ( id) => {
        deleteMedication( INSTRUCTOR,id)
            .then(
                response => {
                    this.listMedications()
                }
            )
    }

    cancel=(e)=>{
        this.setState(() => ({toAccount: true}));
    }


    render() {

        if (this.state.toUpdate === true) {
            this.props.history.push('/medication/${INSTRUCTOR}/update')
        }

        if (this.state.toCreate === true) {
            this.props.history.push('/medication/${INSTRUCTOR}/create')
        }

        if (this.state.toAccount === true) {
            this.props.history.push('/account/doctor/${INSTRUCTOR}')
        }

        return (
            <div className="Medication">
                <h2> Medications </h2>
                <div className="one">
                    <button className="myGreenButton" onClick={() => this.newMedication()}>New </button>
                </div>
                <div>
                    <button  align="right" onClick={() => this.cancel()}>Back</button>
                </div>
                        <table class="paleBlueRows">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Dosage</th>
                                <th>        </th>
                                <th>        </th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                this.state.medications.map(
                                    medication =>
                                        <tr key={medication.id}>
                                            <td>{medication.id}</td>
                                            <td>{medication.name}</td>
                                            <td>{medication.dosage}</td>
                                            <td><button className="myButton" onClick={() => this.updateMedication(medication.id)}>Update</button></td>
                                            <td><button className="myButtonRed"  onClick={() => this.deleteMedication(medication.id)}>Delete</button></td>
                                        </tr>
                                )
                            }
                            </tbody>
                        </table>
                </div>
        )
    }
}

export default Medication;