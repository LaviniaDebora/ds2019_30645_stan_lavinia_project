import React, { Component } from 'react'
import "./UpdateMedication.css";
import {updateMedication,findMedicationById} from "../../service/ApiService";
import AuthenticationService from "../../service/AuthenticationService";

const INSTRUCTOR = AuthenticationService.getLoggedInUserName();

function getValue(key){
    return localStorage.getItem(key);
}

class UpdateMedication extends Component {

    constructor(props){
        super(props);
        this.state ={
            id: '',
            name: '',
            dosage: null,
            toMedication:false,
            toMedicationSave:false
        }
        this.updateMedicationDetails = this.updateMedicationDetails.bind(this);
        this.getMedication = this.getMedication.bind(this);
    }

    componentDidMount() {
        this.getMedication();
    }

    myChangeHandler = (e) => this.setState({ [e.target.name]: e.target.value });

    cancelMedication=(e)=>{
        this.setState(() => ({toMedication: true}));
    }

    updateMedicationDetails = (e) => {
        e.preventDefault();
        const{id, name, dosage}=this.state;
        let medication = {
            id: id,
            name: name,
            dosage: dosage};

        updateMedication(INSTRUCTOR,medication).then(res => {this.setState(() => ({toMedicationSave: true}));});
    }

    getMedication =()=> {
        const id = getValue("medicationId");
        findMedicationById(INSTRUCTOR,id).then((medication) => {
            this.setState({
                id: medication.data.id,
                name: medication.data.name,
                dosage: medication.data.dosage,
            })
        });
    }

    render() {
        if (this.state.toMedication === true) {
            this.props.history.push('/medication/${INSTRUCTOR}/all')
        }
        if (this.state.toMedicationSave === true) {
            this.props.history.push('/medication/${INSTRUCTOR}/all')
        }
        return (
            <form >
                <div className="form-style-2">
                    <div className="form-style-2-heading"> Update medication [{this.state.id}] details</div>

                    <div>
                        <label>Name:  </label>
                        <input
                            className="input-field"
                            type='text'
                            name='name'
                            onChange={this.myChangeHandler}
                        />
                    </div>

                    <div>
                        <label>Dosage: </label>
                        <input
                            className="input-field"
                            type='number'
                            name='dosage'
                            onChange={this.myChangeHandler}
                        />

                    </div>

                    <br/>
                    <br/>
                    <button  className="myButton" onClick={this.updateMedicationDetails}> Save </button>
                    <button className="pad"  onClick={this.cancelMedication}> Cancel </button>
                </div>
            </form>
        );
    }
}

export default UpdateMedication;