import React, { Component } from 'react'
import {addNewCaregiver} from "../../service/ApiService";
import "./NewCaregiver.css";
import AuthenticationService from "../../service/AuthenticationService";

const INSTRUCTOR = AuthenticationService.getLoggedInUserName();

class NewCaregiver extends Component {

    constructor(props){
        super(props);
        this.state ={
            id: '',
            name: '',
            birthdate: null,
            gender: '',
            address: '',
            medicalRecord:[],
            toCaregiver:false,
            toCaregiverSave:false
        }
        this.addCaregiver = this.addCaregiver.bind(this);
    }

    myChangeHandler = (e) =>
        this.setState({ [e.target.name]: e.target.value });

    addCaregiver = (e) => {
        e.preventDefault();

        const{id, name, birthdate, gender, address, medicalRecord}=this.state;
        let caregiver = {
            id: id,
            name: name,
            birthdate: birthdate,
            gender: gender,
            address: address,
            medicalRecord:medicalRecord};

        addNewCaregiver(INSTRUCTOR, caregiver)
            .then(response => {
                this.setState(() => ({toCaregiverSave: true}));
            });
    }

    cancelUpdate=(e)=>{
        this.setState(() => ({toCaregiver: true}));
    }

    render() {

        if (this.state.toCaregiver === true) {
            this.props.history.push('/caregiver/${INSTRUCTOR}/all')
        }
        if (this.state.toCaregiverSave === true) {
            this.props.history.push('/caregiver/${INSTRUCTOR}/all')
        }
        return (
            <form >
                <div className="form-style-2">
                    <div className="form-style-2-heading"> New caregiver registration:</div>

                    <div>
                        <label>Name:  </label>
                        <input
                            className="input-field"
                            type='text'
                            name='name'
                            onChange={this.myChangeHandler}
                        />
                    </div>

                    <div>
                        <label>Birthdate: </label>
                        <input
                            className="input-field"
                            type='date'
                            name='birthdate'
                            onChange={this.myChangeHandler}
                        />

                    </div>

                    <div>
                        <label>Gender: </label>
                        <input
                            className="input-field"
                            type='select'
                            name='gender'
                            onChange={this.myChangeHandler}
                        />
                    </div>

                    <div>
                        <label >Address:  </label>
                        <input
                            className="input-field"
                            type='text'
                            name='address'
                            onChange={this.myChangeHandler}
                        />

                    </div>

                    <div>
                        <label>Medical Record:  </label>
                        <input
                            className="input-field"
                            type='text'
                            name='medicalrecord'
                            onChange={this.myChangeHandler}
                        />
                    </div>

                    <br/>
                    <br/>

                    <button  className="myGreenButton" onClick={this.addCaregiver}> Create </button>
                    <button className="pad"  onClick={this.cancelUpdate}> Cancel </button>
                </div>
            </form>
        );
    }
}

export default NewCaregiver;