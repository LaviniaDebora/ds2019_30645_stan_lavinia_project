import React, { Component } from 'react'
import './Caregiver.css';
import  {getAllCaregivers,deleteCaregiver} from "../../service/ApiService";
import AuthenticationService from "../../service/AuthenticationService";

function storeValue(key,value){
    localStorage.setItem(key, value);
}

function removeValue(key){
    localStorage.removeItem(key);
}

const INSTRUCTOR = AuthenticationService.getLoggedInUserName();

class Caregiver extends Component {
    constructor(props) {
        super(props);
        this.state = {
            toUpdate: false,
            toCreate: false,
            storeId: true,
            caregivers: [],
            toAccount:false,
            toView:false
        };
        this.newCaregiver=this.newCaregiver.bind(this);
        this.updateCaregiver=this.updateCaregiver.bind(this);
        this.listCaregivers = this.listCaregivers.bind(this);
        this.deleteCaregiver=this.deleteCaregiver.bind(this);
    }

    componentDidMount() {
        this.listCaregivers();
    }

    newCaregiver = () => {
        const {storeId}=this.state;
        removeValue("caregiverId");
        this.setState(
            () => (
                {toCreate: true}
            ));
    }

    updateCaregiver = (id) => {
        const {storeId}=this.state;
        storeValue("caregiverId", storeId ? id :" ");
        this.setState(
            () => (
                {toUpdate: true}
            ));
    }

    listCaregivers = () => {
        getAllCaregivers(INSTRUCTOR).
        then(
            response => {
                this.setState(
                    {
                        caregivers: response.data
                    }
                )
            })
    }

    deleteCaregiver= (id) => {
        deleteCaregiver( INSTRUCTOR,id)
            .then(
                response => {
                    this.listCaregivers()
                }
            )
    }

    cancel=(e)=>{
        this.setState(() => ({toAccount: true}));
    }

    viewCaregiver = (id) => {
        const {storeId}=this.state;
        storeValue("viewcareId", storeId ? id :" ");
        this.setState(
            () => (
                {toView: true}
            ));
    }

    render() {

        if (this.state.toUpdate === true) {
            this.props.history.push('/caregiver/${INSTRUCTOR}/update')
        }

        if (this.state.toCreate === true) {
            this.props.history.push('/caregiver/${INSTRUCTOR}/create')
        }

        if (this.state.toAccount === true) {
            this.props.history.push('/account/doctor/${INSTRUCTOR}')
        }

        if (this.state.toView === true) {
            this.props.history.push('/account/caregiver/${INSTRUCTOR}')
        }

        return (
            <div className="Caregiver">
                <h2> Caregivers </h2>
                <div>
                <button className="myGreenButton" onClick={() => this.newCaregiver()}>New</button>
                </div>
                <div>
                    <button  align="right" onClick={() => this.cancel()}>Back</button>
                </div>
                        <table class="paleBlueRows">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Birthdate</th>
                                <th>Gender</th>
                                <th>Address</th>
                                <th>        </th>
                                <th>        </th>
                                <th>        </th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                this.state.caregivers.map(
                                    caregiver =>
                                        <tr key={caregiver.id}>
                                            <td>{caregiver.id}</td>
                                            <td>{caregiver.name}</td>
                                            <td>{caregiver.birthdate}</td>
                                            <td>{caregiver.gender}</td>
                                            <td>{caregiver.address}</td>
                                            <td><button className="myButton" onClick={() => this.updateCaregiver(caregiver.id)}>Update</button></td>
                                            <td><button className="myButtonRed"  onClick={() => this.deleteCaregiver(caregiver.id)}>Delete</button></td>
                                            <td>
                                                <button className="myGreenButton"
                                                        onClick={() => this.viewCaregiver(caregiver.id)}> View
                                                </button>
                                            </td>
                                        </tr>
                                )
                            }
                            </tbody>
                    </table>
                </div>

        )
    }
}

export default Caregiver;