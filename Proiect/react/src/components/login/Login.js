import React, { Component } from 'react'
import AuthenticationService from "../../service/AuthenticationService";
import "./Login.css"
import { findUserByUsername} from"./../../service/ApiService"

const INSTRUCTOR = AuthenticationService.getLoggedInUserName()

class Login extends Component {

    constructor(props) {
        super(props)

        this.state = {
            userFromApi: null,
            username: '',
            password: '',
            role:'',
            hasLoginFailed: false,
            toPatientAccount:false,
            toCaregiverAccount:false,
            toDoctorAccount:false

        }

        this.handleChange = this.handleChange.bind(this)
        this.loginClicked = this.loginClicked.bind(this)
    }

    handleChange(event) {
        this.setState(
            {
                [event.target.name]
                    : event.target.value
            }
        )
    }

    loginClicked() {

        AuthenticationService
            .executeBasicAuthenticationService(this.state.username, this.state.password)
            .then(() => {
                AuthenticationService.registerSuccessfulLogin(this.state.username, this.state.password)
                let user = findUserByUsername(INSTRUCTOR, this.state.username);
                console.log(user);
                if(user.role="PATIENT") {
                    console.log(user.role);
                    this.props.history.push('/account/patient')
                }

                if (user.role = "CAREGIVER") {
                    console.log(user.role);
                        this.props.history.push('/account/caregiver')
                }

                    if(user.role="DOCTOR") {
                        console.log(user.role);
                            this.props.history.push('/account/doctor/${INSTRUCTOR}')
                    }

            }).catch(() => {
            this.setState({ hasLoginFailed: true })
        })
    }

    render() {
        return (

            <div>
                <div className="Login">
                    <h1>Login</h1>
                    <p>Username: <input
                        type='text'
                        name='username'
                        value={this.state.username}
                        onChange={this.handleChange}
                    />

                    </p>

                    <p>Password: <input
                        type='text'
                        name='password'
                        value={this.state.password}
                        onChange={this.handleChange}
                    />
                    </p>
                    <button  onClick={this.loginClicked}>Login</button>
                </div>
            </div>

        )
    }
}

export default Login