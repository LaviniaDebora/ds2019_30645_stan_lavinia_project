import React, { Component } from 'react'
import "./NewPatient.css";
import {addNewPatient} from "../../service/ApiService";
import AuthenticationService from "../../service/AuthenticationService";

const INSTRUCTOR = AuthenticationService.getLoggedInUserName();

class NewPatient extends Component {

    constructor(props){
        super(props);
        this.state ={
            id: '',
            name: '',
            birthdate: null,
            gender: '',
            address: '',
            medicalRecord:[],
            toPatient:false,
            toPatientSave:false
        }
        this.addPatient = this.addPatient.bind(this);

    }

    myChangeHandler = (e) =>
        this.setState({ [e.target.name]: e.target.value });

    addPatient = (e) => {
        e.preventDefault();

        const{id, name, birthdate, gender, address, medicalRecord}=this.state;
        let patient = {
            id: id,
            name: name,
            birthdate: birthdate,
            gender: gender,
            address: address,
            medicalRecord:medicalRecord};

        addNewPatient(INSTRUCTOR, patient)
            .then(response => {
                      this.setState(() => ({toPatientSave: true}))})
    }

    cancelUpdate=(e)=>{
        this.setState(() => ({toPatient: true}));
    }

    render() {

        if (this.state.toPatient === true) {
            this.props.history.push('/patient/${INSTRUCTOR}/all')

        }
        if (this.state.toPatientSave === true) {
            this.props.history.push('/patient/${INSTRUCTOR}/all')
        }
        return (
            <form >
                <div className="form-style-2">
                    <div className="form-style-2-heading"> New user registration:</div>

                    <div>
                        <label>Name:  </label>
                        <input
                            className="input-field"
                            type='text'
                            name='name'
                            onChange={this.myChangeHandler}
                        />
                    </div>

                    <div>
                        <label>Birthdate: </label>
                        <input
                            className="input-field"
                            type='date'
                            name='birthdate'
                            onChange={this.myChangeHandler}
                        />

                    </div>

                    <div>
                        <label>Gender: </label>
                        <input
                            className="input-field"
                            type='select'
                            name='gender'
                            onChange={this.myChangeHandler}
                        />
                    </div>

                    <div>
                        <label >Address:  </label>
                        <input
                            className="input-field"
                            type='text'
                            name='address'
                            onChange={this.myChangeHandler}
                        />

                    </div>

                    <div>
                        <label>Medical Record:  </label>
                        <input
                            className="input-field"
                            type='text'
                            name='medicalrecord'
                            onChange={this.myChangeHandler}
                        />
                    </div>

                    <br/>
                    <br/>

                    <button  className="myGreenButton" onClick={this.addPatient}> Create </button>
                    <button className="pad"  onClick={this.cancelUpdate}> Cancel </button>
                </div>
            </form>
        );
    }
}

export default NewPatient;