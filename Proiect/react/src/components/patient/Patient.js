import React, { Component ,useState } from 'react'
import './Patient.css';
import  {getAllPatients,deletePatient} from "../../service/ApiService";
import AuthenticationService from "../../service/AuthenticationService";

function storeValue(key,value){
    localStorage.setItem(key, value);
}

function removeValue(key){
    localStorage.removeItem(key);
}

const INSTRUCTOR = AuthenticationService.getLoggedInUserName();

class Patient extends Component {

    constructor(props) {
        super(props);
        this.state = {
            toUpdate: false,
            toCreate: false,
            toMedicationPlan:false,
            storeId: true,
            patients: [],
            toAccount: false,
            toView:false
        };
        this.newPatient=this.newPatient.bind(this);
        this.updatePatient=this.updatePatient.bind(this);
        this.listPatients = this.listPatients.bind(this);
        this.deletePatient=this.deletePatient.bind(this);
    }

    componentDidMount() {
        this.listPatients();
    }

    newPatient = () => {
        const {storeId}=this.state;
        removeValue("patientId");
        this.setState(
            () => (
                {toCreate: true}
            ));
    }

    newMedicationPlan = () => {
        const {storeId}=this.state;
        removeValue("medicationplanId");
        this.setState(
            () => (
                {toMedicationPlan: true}
            ));
    }

    updatePatient = (id) => {
        const {storeId}=this.state;
        storeValue("patientId", storeId ? id :" ");
        this.setState(
            () => (
                {toUpdate: true}
            ));

    }

    viewPatient = (id) => {
        const {storeId}=this.state;
        storeValue("viewId", storeId ? id :" ");
        this.setState(
            () => (
                {toView: true}
            ));

    }

    listPatients = () => {
        getAllPatients(INSTRUCTOR)
            .then(
            response => {
                this.setState(
                     {
                        patients: response.data
                        }
                     )
            })
    }

    deletePatient= (id) => {
        deletePatient(INSTRUCTOR,id)
            .then(
                response => {
                    this.listPatients()
                }
            )
    }

    cancel=(e)=>{
        this.setState(() => ({toAccount: true}));
    }

    render() {

        if (this.state.toUpdate === true) {
            this.props.history.push('/patient/${INSTRUCTOR}/update')
        }

        if (this.state.toCreate === true) {
            this.props.history.push('/patient/${INSTRUCTOR}/create')
        }

        if (this.state.toMedicationPlan === true) {
            this.props.history.push('/medicationplan/${INSTRUCTOR}/create')
        }

            if (this.state.toAccount === true) {
                this.props.history.push('/account/doctor/${INSTRUCTOR}')
        }

        if (this.state.toView === true) {
                this.props.history.push('/account/patient/${INSTRUCTOR}')
        }

        return (
            <div className="Patient">
                <h2> Patients </h2>
                <div>
                    <button className="myGreenButton" onClick={() => this.newPatient()}>New</button>
                </div>
                <div>
                    <button  align="right" onClick={() => this.cancel()}>Back</button>
                </div>
                <div>
                    <table class="paleBlueRows">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Birthdate</th>
                            <th>Gender</th>
                            <th>Address</th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            this.state.patients.map(
                                patient =>
                                    <tr key={patient.id}>
                                        <td>{patient.id}</td>
                                        <td>{patient.name}</td>
                                        <td>{patient.birthdate}</td>
                                        <td>{patient.gender}</td>
                                        <td>{patient.address}</td>
                                        <td>
                                            <button className="myButton"
                                                    onClick={() => this.updatePatient(patient.id)}>Update
                                            </button>
                                        </td>
                                        <td>
                                            <button className="myButtonRed"
                                                    onClick={() => this.deletePatient(patient.id)}>Delete
                                            </button>
                                        </td>
                                        <td>
                                            <button className="myPurppleButton"
                                                    onClick={() => this.newMedicationPlan()}  > +MedicalPlan
                                            </button>
                                        </td>
                                        <td>
                                            <button className="myGreenButton"
                                                    onClick={() => this.viewPatient(patient.id)}> View
                                            </button>
                                        </td>
                                    </tr>
                            )
                        }
                        </tbody>
                    </table>


                </div>
            </div>
        )
    }
}

export default Patient;