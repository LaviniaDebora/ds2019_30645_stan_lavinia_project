import React, { Component } from 'react'
import "./UpdatePatient.css";
import {updatePatient,findPatientById} from "../../service/ApiService";
import AuthenticationService from "../../service/AuthenticationService";

const INSTRUCTOR = AuthenticationService.getLoggedInUserName();

function getValue(key){
    return localStorage.getItem(key);
}

class UpdatePatient extends Component {

    constructor(props){
        super(props);
        this.state ={
            id: '',
            name: '',
            birthdate: null,
            gender: '',
            address: '',
            medicalRecord:[],
            toPatient:false,
            toPatientSave:false
        }
        this.updatePatientDetails = this.updatePatientDetails.bind(this);
        this.getPatient = this.getPatient.bind(this);
    }

    componentDidMount() {
        this.getPatient();
    }

    myChangeHandler = (e) => this.setState({ [e.target.name]: e.target.value });

    cancelUpdate=(e)=>{this.setState(() => ({toPatient: true}));}

    updatePatientDetails = (e) => {
        e.preventDefault();
        const{id, name, birthdate, gender, address, medicalRecord}=this.state;
        let patient = {
            id: id,
            name: name,
            birthdate: birthdate,
            gender: gender,
            address: address,
            medicalRecord:medicalRecord};
        updatePatient(INSTRUCTOR, patient)
            .then(response => {
                this.setState(() => ({toPatientSave: true}))})
    }

    getPatient =()=> {
       const id = getValue("patientId");
        findPatientById(INSTRUCTOR,id).then((patient) => {
                this.setState({
                    id: patient.data.id,
                    name: patient.data.name,
                    birthdate: patient.data.birthdate,
                    gender: patient.data.gender,
                    address: patient.data.address,
                    medicalRecord: patient.data.medicalRecord,
                })
            });
    }

    render() {
        if (this.state.toPatient === true) {
            this.props.history.push('/patient/${INSTRUCTOR}/all')
        }
        if (this.state.toPatientSave === true) {
            this.props.history.push('/patient/${INSTRUCTOR}/all')
        }
        return (
            <form >
                <div className="form-style-2">
                    <div className="form-style-2-heading"> Update user [{this.state.id}] details</div>

                    <div>
                    <label>Name:  </label>
                        <input
                            className="input-field"
                            type='text'
                            name='name'
                            onChange={this.myChangeHandler}
                        />
                    </div>

                    <div>
                    <label>Birthdate: </label>
                        <input
                            className="input-field"
                            type='date'
                            name='birthdate'
                            onChange={this.myChangeHandler}
                        />

                    </div>

                    <div>
                    <label>Gender: </label>
                        <input
                            className="input-field"
                            type='select'
                            name='gender'
                            onChange={this.myChangeHandler}
                        />
                    </div>

                    <div>
                    <label >Address:  </label>
                        <input
                            className="input-field"
                            type='text'
                            name='address'
                            onChange={this.myChangeHandler}
                        />

                    </div>

                    <div>
                    <label>Medical Record:  </label>
                        <input
                            className="input-field"
                            type='text'
                            name='medicalrecord'
                            onChange={this.myChangeHandler}
                        />
                    </div>

                    <br/>
                    <br/>
                    <button  className="myButton" onClick={this.updatePatientDetails}> Save </button>
                    <button className="pad"  onClick={this.cancelUpdate}> Cancel </button>
                </div>
            </form>
        );
    }
}

export default UpdatePatient;