import React, { Component } from 'react'
import "./NewMedicationPlan.css";
import {addNewMedicationPlan} from "../../service/ApiService";
import AuthenticationService from "../../service/AuthenticationService";

const INSTRUCTOR = AuthenticationService.getLoggedInUserName();

class NewMedicationPlan extends Component {

    constructor(props){
        super(props);
        this.state ={
            id: '',
            start_date:'',
            end_date: '',
            toPatients:false,
            toPatientsSave:false
        }
        this.addMedicationPlan = this.addMedicationPlan.bind(this);
    }

    myChangeHandler = (e) =>
        this.setState({ [e.target.name]: e.target.value });

    addMedicationPlan = (e) => {
        e.preventDefault();
        const{id, start_date, end_date}=this.state;
        let medicationplan = {
            id: id,
            start_date: start_date,
            end_date: end_date};

        addNewMedicationPlan(INSTRUCTOR, medicationplan)
            .then(response => {
                this.setState(() => ({toPatientsSave: true}));
            });
    }

    cancelMedicationPlan=(e)=>{
        this.setState(() => ({toPatients: true}));
    }

    render() {

        if (this.state.toPatients === true) {
            this.props.history.push('/patient/${INSTRUCTOR}/all')
        }
        if (this.state.toPatientsSave === true) {
            this.props.history.push('/patient/${INSTRUCTOR}/all')
        }
        return (
            <form >
                <div className="form-style-2">
                    <div className="form-style-2-heading"> New medication plan :</div>

                    <div>
                        <label>Start Date:  </label>
                        <input
                            className="input-field"
                            type='date'
                            name='start_date'
                            onChange={this.myChangeHandler}
                        />
                    </div>

                    <div>
                        <label>End Date: </label>
                        <input
                            className="input-field"
                            type='date'
                            name='end_date'
                            onChange={this.myChangeHandler}
                        />
                    </div>
                    <br/>
                    <br/>

                    <button  className="myPurppleButton" onClick={this.addMedicationPlan}> Create </button>
                    <button className="pad"  onClick={this.cancelMedicationPlan}> Cancel </button>
                </div>
            </form>
        );
    }
}

export default NewMedicationPlan;