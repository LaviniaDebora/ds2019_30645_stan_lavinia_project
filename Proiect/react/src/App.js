import React, { Component } from 'react';
import {BrowserRouter as Router, Link, Route, Switch} from 'react-router-dom'
import Login from "./components/login/Login";
import Logout from './components/login/Logout';
import AuthenticatedRoute from './components/login/AuthenticatedRoute';
import Patient from "./components/patient/Patient";
import AuthenticationService from "./service/AuthenticationService";
import NewPatient from "./components/patient/NewPatient";
import UpdatePatient from "./components/patient/UpdatePatient";
import MyDoctorAccount from "./components/accounts/MyDoctorAccount"
import Caregiver from "./components/caregiver/Caregiver";
import NewCaregiver from "./components/caregiver/NewCaregiver";
import UpdateCaregiver from "./components/caregiver/UpdateCaregiver";
import Medication from "./components/medication/Medication";
import NewMedication from "./components/medication/NewMedication";
import UpdateMedication from "./components/medication/UpdateMedication";
import NewMedicationPlan from "./components/medicationPlan/NewMedicationPlan";
import Background from "./medical.jpg"
import MyPatientAccount from "./components/accounts/MyPatientAccount";
import MyCaregiverAccount from "./components/accounts/MyCaregiverAccount";

const INSTRUCTOR = AuthenticationService.getLoggedInUserName();

var sectionStyle = {
    backgroundImage: "url(" + Background + ")",
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "100%"
};

class App extends Component {

    render() {

        const isUserLoggedIn = AuthenticationService.isUserLoggedIn();

        return (
            <section style={ sectionStyle }>
            <>
                <Router>
                    <header>
                        <div>
                            {!isUserLoggedIn && <button><Link to="/login"> Log </Link></button>}
                            {isUserLoggedIn && <button><Link to="/logout" onClick={AuthenticationService.logout}> Log </Link></button>}
                        </div>
                    </header>
                    <Switch>
                        <Route path="/" exact component={Login}/>
                        <Route path="/login" exact component={Login}/>
                        <AuthenticatedRoute path="/logout" exact component={Logout}/>
                        <AuthenticatedRoute path="/account/doctor/${INSTRUCTOR}" exact component={MyDoctorAccount}/>
                        <AuthenticatedRoute path="/account/patient/${INSTRUCTOR}" exact component={MyPatientAccount}/>
                        <AuthenticatedRoute path="/account/caregiver/${INSTRUCTOR}" exact component={MyCaregiverAccount}/>

                        <AuthenticatedRoute path="/patient/${INSTRUCTOR}/all" exact component={Patient}/>
                        <AuthenticatedRoute path="/patient/${INSTRUCTOR}/create" exact component={NewPatient}/>
                        <AuthenticatedRoute path="/patient/${INSTRUCTOR}/update" exact component={UpdatePatient}/>
                        <AuthenticatedRoute path="/patient/${INSTRUCTOR}/delete"/>

                        <AuthenticatedRoute path="/caregiver/${INSTRUCTOR}/all" exact component={Caregiver}/>
                        <AuthenticatedRoute path="/caregiver/${INSTRUCTOR}/create" exact component={NewCaregiver}/>
                        <AuthenticatedRoute path="/caregiver/${INSTRUCTOR}/update" exact component={UpdateCaregiver}/>
                        <AuthenticatedRoute path="/caregiver/${INSTRUCTOR}/delete"/>

                        <AuthenticatedRoute path="/medication/${INSTRUCTOR}/all" exact component={Medication}/>
                        <AuthenticatedRoute path="/medication/${INSTRUCTOR}/create" exact component={NewMedication}/>
                        <AuthenticatedRoute path="/medication/${INSTRUCTOR}/update" exact component={UpdateMedication}/>
                        <AuthenticatedRoute path="/medication/${INSTRUCTOR}/delete"/>

                        <AuthenticatedRoute path="/medicationplan/${INSTRUCTOR}/create" exact component={NewMedicationPlan}/>

                    </Switch>

                </Router>
            </>
            </section>
        )
    }
}

export default App