package com.example.spring.consumer;


public class ActivityBuilder {

    public ActivityBuilder() {
    }

    public static ActivityDTO generateDTOFromEntity(Activity activity){
        return new ActivityDTO(
                activity.getId(),
                activity.getActivity_name(),
                activity.getStart_date(),
                activity.getEnd_date());
    }

    public static Activity generateEntityFromDTO(ActivityDTO activityDTO){
        return new Activity(
                activityDTO.getId(),
                activityDTO.getActivity_name(),
                activityDTO.getStart_date(),
                activityDTO.getEnd_date());
    }
}
