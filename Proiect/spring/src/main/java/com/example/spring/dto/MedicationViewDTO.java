package com.example.spring.dto;

import java.sql.Time;
import java.util.List;
import java.util.TimeZone;

public class MedicationViewDTO {

    private Integer id;
    private String name;
    private List<String> sideEffects;
    private Integer dosage;
    private String start;
    private String end;
    private Boolean taken;

    public MedicationViewDTO(Integer id, String name, List<String> sideEffects, Integer dosage, String start, String end, Boolean taken) {
        this.id=id;
        this.name=name;
        this.sideEffects=sideEffects;
        this.dosage=dosage;
        this.start=start;
        this.end=end;
        this.taken=taken;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(List<String> sideEffects) {
        this.sideEffects = sideEffects;
    }

    public Integer getDosage() {
        return dosage;
    }

    public void setDosage(Integer dosage) {
        this.dosage = dosage;
    }

    public String getStartTime() {
        return start;
    }

    public void setStartTime(String start) {
        this.start = start;
    }

    public String getEndTime() {
        return start;
    }

    public void setEndTime(String start) {
        this.start = start;
    }

    public Boolean getTaken() {
        return taken;
    }

    public void setTaken(Boolean taken) {
        this.taken = taken;
    }
}
