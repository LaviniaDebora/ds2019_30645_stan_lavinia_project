package com.example.spring.controller;

import com.example.spring.dto.DoctorDTO;
import com.example.spring.dto.DoctorViewDTO;
import com.example.spring.services.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/doctor")
public class DoctorController {

    private final DoctorService doctorService;

    @Autowired
    public DoctorController(DoctorService doctorService) {
        this.doctorService = doctorService;
    }

    @PostMapping()
    public Integer insertDoctorDTO(@RequestBody DoctorDTO doctorDTO){
        return doctorService.insert(doctorDTO);
    }

    @GetMapping()
    public List<DoctorViewDTO> findAll(){
        return doctorService.findAll();
    }

    @GetMapping(value = "/{id}")
    public DoctorViewDTO findById(@PathVariable("id") Integer id){
        return doctorService.findDoctorById(id);
    }

    @PutMapping()
    public Integer updateUser(@RequestBody DoctorDTO doctorDTO) {
        return doctorService.update(doctorDTO);
    }

    @DeleteMapping()
    public void delete(@RequestBody DoctorViewDTO doctorViewDTO){
        doctorService.delete(doctorViewDTO);
    }
}
