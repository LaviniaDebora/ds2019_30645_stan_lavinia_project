package com.example.spring.rmi;



import java.rmi.Remote;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.ArrayList;

public interface InterfaceRMI extends Remote{

    ArrayList<Med> getMedicationPlan() throws RemoteException, SQLException ;

    public void sendMessage(String text) throws RemoteException;

    public String getMessage(String text) throws RemoteException;

}