package com.example.spring.entities;

import javax.persistence.*;
import java.io.Serializable;

import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "medication")
public class Medication {
    private static final long serialVersionUID = 1190476516911661470L;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "name", length = 100)
    private String name;

    @Column(name = "side_effects_list")
    @ElementCollection(targetClass=String.class)
    private List<String> sideEffects;

    @Column(name = "dosage")
    private Integer dosage;

    @Column(name = "startTime")
    private String start;

    @Column(name = "endTime")
    private String end;

    @Column(name = "taken")
    private Boolean taken;

    @ManyToOne
    private MedicationPlan medicationPlan;

    public Medication(){
    }

    public Medication(Integer id, String name, List<String> sideEffects, Integer dosage, String start, String end, Boolean taken){
        this.id=id;
        this.name=name;
        this.sideEffects=sideEffects;
        this.dosage=dosage;
        this.start=start;
        this.end=end;
        this.taken=taken;

    }

    public Medication(Integer id, String name, Integer dosage, String start, String end, Boolean taken){
        this.id=id;
        this.name=name;
        this.dosage=dosage;
        this.start=start;
        this.end=end;
        this.taken=taken;

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(List<String> sideEffects) {
        this.sideEffects = sideEffects;
    }

    public Integer getDosage() {
        return dosage;
    }

    public void setDosage(Integer dosage) {
        this.dosage = dosage;
    }

    public String getStartTime() {
        return start;
    }

    public void setStartTime(String start) {
        this.start = start;
    }

    public String getEndTime() {
        return end;
    }

    public void setEndTime(String end) {
        this.end = end;
    }

    public Boolean getTaken() {
        return taken;
    }

    public void setTaken(Boolean taken) {
        this.taken = taken;
    }

}
