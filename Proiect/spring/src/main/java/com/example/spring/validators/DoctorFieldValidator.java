package com.example.spring.validators;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.example.spring.dto.DoctorDTO;
import com.example.spring.errorhandler.IncorrectParameterException;
import java.util.List;
import java.util.ArrayList;

public class DoctorFieldValidator {

    private static final Log LOGGER = LogFactory.getLog(DoctorFieldValidator.class);

    public static void validateInsertOrUpdate(DoctorDTO doctorDTO) {
        List<String> errors = new ArrayList<>();
        if (doctorDTO == null) {
            errors.add("doctorDTO is null");
            throw new IncorrectParameterException(DoctorDTO.class.getSimpleName(), errors);
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(DoctorFieldValidator.class.getSimpleName(), errors);
        }
    }
}
