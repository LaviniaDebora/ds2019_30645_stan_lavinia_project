package com.example.spring.consumer;

import com.example.spring.entities.Patient;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "activity")
public class Activity {

    @Column(name = "activity_name", nullable = false, length = 100)
    @JsonProperty
    private String activity;

    @Column(name = "start_date", nullable = false, length = 100)
    @JsonProperty
    private String start;

    @Column(name = "end_date", nullable = false, length = 100)
    @JsonProperty
    private String end;

    @JsonIgnore
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    private Patient activitypatient;

    public Activity(Integer id, String activity_name, String start_date, String end_date){
        this.id=id;
        this.activity=activity_name;
        this.start=start_date;
        this.end=end_date;
    }

    public Activity(){}

    public Integer getId(){
        return this.id;
    }

    public void setID(Integer id){
        this.id=id;
    }

    public String getActivity_name(){
        return this.activity;
    }

    public void setActivity_name(String activity_name) {
        this.activity = activity_name;
    }

    public String getEnd_date() {
        return this.end;
    }

    public void setEnd_date(String date){
        this.end=date;
    }

    public String getStart_date(){
        return this.start;
    }

    public void setStart_date(String date){
        this.start=date;
    }
}
