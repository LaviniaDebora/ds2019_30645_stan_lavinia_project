package com.example.spring.dto;

import com.example.spring.entities.Patient;
import java.util.Date;
import java.util.List;

public class CaregiverViewDTO {

    private Integer id;
    private String name;
    private Date birthdate;
    private String gender;
    private String address;
    private List<Patient> patientsList;

    public CaregiverViewDTO(){}

    public CaregiverViewDTO(Integer id, String name, Date birthdate, String gender, String address){
        this.id=id;
        this.name=name;
        this.birthdate=birthdate;
        this.gender=gender;
        this.address=address;

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthdate(){return birthdate; }

    public void setBirthdate(Date birthdate){this.birthdate=birthdate; }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Patient> getPatientsList() { return patientsList; }

    public void setPatientsList(List<Patient> patientsList) { this.patientsList = patientsList; }

}
