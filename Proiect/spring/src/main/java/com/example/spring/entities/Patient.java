package com.example.spring.entities;

import com.example.spring.consumer.Activity;
import com.example.spring.consumer.Notification;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "patient")
public class Patient {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "name", length = 100)
    private String name;

    @Column(name = "birth_date")
    private Date birthdate;

    @Column(name = "gender", length = 50)
    private String gender;

    @Column(name = "address")
    private String address;

    @Column(name = "medical_record")
    @ElementCollection(targetClass=String.class)
    private List<String> medicalRecord;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    private Caregiver caregiver;

    @JsonIgnore
    @OneToOne(mappedBy = "patient")
    private MedicationPlan medicationPatient;

    @OneToOne
    @JoinColumn(name="patient_user_id")
    private User patientUser;


    @OneToMany(mappedBy = "activitypatient", cascade = CascadeType.ALL)
    private List<Activity> activityList;

    @OneToMany(mappedBy = "notificationpatient", cascade = CascadeType.ALL)
    private List<Notification> notificationsList;

    public Patient(){}

    public Patient(Integer id, String name, Date birthdate, String gender, String address,List<String> medicalRecord){
        this.id=id;
        this.name=name;
        this.birthdate=birthdate;
        this.gender=gender;
        this.address=address;
        this.medicalRecord=medicalRecord;

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthdate(){return birthdate; }

    public void setBirthdate(Date birthdate){this.birthdate=birthdate; }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<String> getMedicalRecord() { return medicalRecord; }

    public void setMedicalRecord(List<String> medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

}
