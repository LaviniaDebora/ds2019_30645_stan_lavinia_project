package com.example.spring.controller;

import com.example.spring.dto.CaregiverDTO;
import com.example.spring.dto.CaregiverViewDTO;
import com.example.spring.services.CaregiverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/caregiver")
public class CaregiverController {

    private final CaregiverService caregiverService;

    @Autowired
    public CaregiverController(CaregiverService caregiverService) {
        this.caregiverService = caregiverService;
    }

    @PostMapping(value="/{username}/create")
    public Integer insertCaregiverDTO(@PathVariable String username,@RequestBody CaregiverDTO caregiverDTO){
        return caregiverService.insert(caregiverDTO);
    }

    @GetMapping(value="/{username}/all")
    public List<CaregiverViewDTO> findAll(@PathVariable String username){
        return caregiverService.findAll();
    }

    @GetMapping(value = "/{username}/{id}")
    public CaregiverViewDTO findById(@PathVariable String username, @PathVariable("id") Integer id){
        return caregiverService.findCaregiverById(id);
    }

    @PutMapping(value="/{username}/update")
    public Integer updateUser(@PathVariable String username, @RequestBody CaregiverDTO caregiverDTO) {
        return caregiverService.update(caregiverDTO);
    }

    @DeleteMapping(value = "/{username}/delete/{id}")
    public void delete(@PathVariable String username,@PathVariable("id") Integer id){
        caregiverService.delete(id);
    }
}
