package com.example.spring.dto.builders;

import com.example.spring.dto.UserViewDTO;
import com.example.spring.entities.User;

public class UserViewBuilder {

    public static UserViewDTO generateDTOFromEntity(User user){
        return new UserViewDTO(
                user.getId(),
                user.getUsername(),
                user.getRole());
    }

    public static User generateEntityFromDTO( UserViewDTO userViewDTO){
        return new User(
                userViewDTO.getId(),
                userViewDTO.getUsername(),
                userViewDTO.getRole());
    }
}
