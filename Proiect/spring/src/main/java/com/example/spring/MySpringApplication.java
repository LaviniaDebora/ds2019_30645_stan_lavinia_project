package com.example.spring;

import com.example.server.ActivityProvider;
import com.example.server.MyRules;
import com.example.spring.consumer.QueueConnection;
import com.example.spring.rmi.InterfaceRMI;
import com.example.spring.rmi.Server;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.xml.ws.Endpoint;
import java.io.IOException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.TimeZone;
import java.util.concurrent.TimeoutException;

@SpringBootApplication
public class MySpringApplication {

	public static void main(String[] args) throws IOException, TimeoutException {

		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
		SpringApplication.run(MySpringApplication.class, args);
		QueueConnection connection = new QueueConnection("daily_activities","localhost");
		connection.receiveMessage();
		try {
			Server obj = new Server();
			InterfaceRMI stub = (InterfaceRMI) UnicastRemoteObject.exportObject(obj, 0);
			Registry registry = LocateRegistry.createRegistry(8081);
			registry.bind("//localhost:8081/InterfaceRMI",  stub);
			registry.rebind("//localhost:8081/InterfaceRMI", stub);
			System.err.println("Server ready");
		} catch (Exception e) {
			System.err.println("Server exception: " + e.toString());
			e.printStackTrace();
		}


		Endpoint myendpoint = Endpoint.create(new ActivityProvider());
		myendpoint.publish("http://127.0.0.1:8070/myactivity");

		MyRules rules = new MyRules();
		rules.checkRules(1);
	}

}
