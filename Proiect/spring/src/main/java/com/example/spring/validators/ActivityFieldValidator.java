package com.example.spring.validators;

import com.example.spring.consumer.ActivityDTO;
import com.example.spring.dto.PatientDTO;
import com.example.spring.errorhandler.IncorrectParameterException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;

public class ActivityFieldValidator {

    private static final Log LOGGER = LogFactory.getLog(ActivityFieldValidator.class);

    public static void validateInsertOrUpdate(ActivityDTO activityDTO) {
        List<String> errors = new ArrayList<>();
        if (activityDTO == null) {
            errors.add("activityDTO is null");
            throw new IncorrectParameterException(ActivityDTO.class.getSimpleName(), errors);
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(ActivityFieldValidator.class.getSimpleName(), errors);
        }
    }
}
