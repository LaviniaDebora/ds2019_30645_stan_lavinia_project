package com.example.spring.validators;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.example.spring.dto.MedicationDTO;
import com.example.spring.errorhandler.IncorrectParameterException;
import java.util.List;
import java.util.ArrayList;

public class MedicationFieldValidator {
    private static final Log LOGGER = LogFactory.getLog(MedicationFieldValidator.class);

    public static void validateInsertOrUpdate(MedicationDTO medicationDTO) {
        List<String> errors = new ArrayList<>();
        if (medicationDTO == null) {
            errors.add("medicationDTO is null");
            throw new IncorrectParameterException(MedicationDTO.class.getSimpleName(), errors);
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(MedicationFieldValidator.class.getSimpleName(), errors);
        }
    }
}
