package com.example.spring.rmi;

import java.rmi.RemoteException;
import java.sql.*;
import java.util.ArrayList;

public class Server implements InterfaceRMI {

    private static final long serialVersionUID = 1L;
    public Server(){}

    @Override
    public void sendMessage(String s) throws RemoteException {
        System.out.println(s);
    }

    @Override
    public String getMessage(String text) throws RemoteException {
        return "Message: " + text;
    }

    public  ArrayList<Med> getMedicationPlan() throws RemoteException, SQLException {
        String url;
        Connection myConn = null;
        ArrayList<Med> list= new ArrayList<Med>();
        try {
            url="jdbc:mysql://127.0.0.1:3306/mydb?useSSL=false";
            myConn = DriverManager.getConnection(url,"root","starwars");
            Statement myStm=myConn.createStatement();
            ResultSet myRes =myStm.executeQuery("select id from medication_plan where start_date = \"1990-01-01\"");
            int id = 0;

            while (myRes.next())
            {
                id = myRes.getInt(1);
            }

            String sql = "select * from medication where medication_plan_id="+id;
            ResultSet rez = myStm.executeQuery(sql);
            int dosage;
            String start_time;
            String end_time;
            String name;
            Boolean taken;
            int idd;

            while (rez.next())
            {
                idd = rez.getInt("id");
                dosage = rez.getInt("dosage");
                start_time = rez.getString("start_time");
                end_time = rez.getString("end_time");
                name = rez.getString("name");
                taken = rez.getBoolean("taken");
                Med med = new Med(idd,name,dosage,start_time,end_time,taken);
                list.add(med);
            }
            myConn.close();

        }
        catch (Exception e) {
            System.out.println(e.toString());
        }
        return list;
    }
}
