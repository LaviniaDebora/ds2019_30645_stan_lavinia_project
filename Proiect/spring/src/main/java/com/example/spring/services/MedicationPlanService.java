package com.example.spring.services;

import com.example.spring.dto.*;
import com.example.spring.dto.builders.*;
import com.example.spring.entities.MedicationPlan;
import com.example.spring.repositories.MedicationPlanRepository;
import com.example.spring.validators.MedicationPlanFieldValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.spring.errorhandler.ResourceNotFoundException;
import java.util.stream.Collectors;
import java.util.List;
import java.util.Optional;

@Service
public class MedicationPlanService {
    private final MedicationPlanRepository medicationPlanRepository;

    @Autowired
    public MedicationPlanService(MedicationPlanRepository medicationPlanRepository) {
        this.medicationPlanRepository = medicationPlanRepository;
    }

    public Integer insert(MedicationPlanDTO medicationPlanDTO) {
        MedicationPlanFieldValidator.validateInsertOrUpdate(medicationPlanDTO);
        return medicationPlanRepository.save(MedicationPlanBuilder.generateEntityFromDTO(medicationPlanDTO)).getId();
    }

    public List<MedicationPlanViewDTO> findAll(){
        List<MedicationPlan> medicationPlans = medicationPlanRepository.getAllOrdered();
        return medicationPlans.stream().map(MedicationPlanViewBuilder::generateDTOFromEntity).collect(Collectors.toList());
    }

    public MedicationPlanViewDTO findMedicationPlanById(Integer id){
        Optional<MedicationPlan> medicationPlan  = medicationPlanRepository.findById(id);
        if (!medicationPlan.isPresent()) {
            throw new ResourceNotFoundException("MedicationPlan", "med id", id);
        }
        return MedicationPlanViewBuilder.generateDTOFromEntity(medicationPlan.get());
    }

}
