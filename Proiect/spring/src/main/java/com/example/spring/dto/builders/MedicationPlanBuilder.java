package com.example.spring.dto.builders;

import com.example.spring.dto.MedicationPlanDTO;
import com.example.spring.entities.MedicationPlan;

public class MedicationPlanBuilder {

    public MedicationPlanBuilder() {
    }

    public static MedicationPlanDTO generateDTOFromEntity(MedicationPlan medicationPlan){
        return new MedicationPlanDTO(
                medicationPlan.getId(),
                medicationPlan.getStartDate(),
                medicationPlan.getEndDate());
//                medicationPlan.getMedicationList());
    }

    public static MedicationPlan generateEntityFromDTO(MedicationPlanDTO medicationPlanDTO){
        return new MedicationPlan(
                medicationPlanDTO.getId(),
                medicationPlanDTO.getStartDate(),
                medicationPlanDTO.getEndDate());
//                medicationPlanDTO.getMedicationList());
    }
}
