package com.example.spring.controller;

import com.example.spring.dto.UserDTO;
import com.example.spring.dto.UserViewDTO;
import com.example.spring.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/user")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping()
    public List<UserViewDTO> findAll(){
        return userService.findAll();
    }

    @GetMapping(value = "/{id}")
    public UserViewDTO findById(@PathVariable("id") Integer id){
        return userService.findUserById(id);
    }


    @GetMapping(value = "/{username}/{name}")
    public UserViewDTO findByUsername(@PathVariable String username, @PathVariable String name){
        return userService.findUserByUsername(name);
    }
}
