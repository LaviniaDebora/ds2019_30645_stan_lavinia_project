package com.example.spring.dto;

public class UserViewDTO {

    private Integer id;
    private String username;
    private String role;

    public UserViewDTO(Integer id, String username, String role){
        this.id=id;
        this.username=username;
        this.role=role;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsernameName(String username) {
        this.username = username;
    }

    public String getRole() { return role; }

    public void setRole(String role) { this.role = role; }
}
