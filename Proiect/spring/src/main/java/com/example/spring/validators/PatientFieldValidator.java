package com.example.spring.validators;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.example.spring.dto.PatientDTO;
import com.example.spring.errorhandler.IncorrectParameterException;
import java.util.List;
import java.util.ArrayList;


public class PatientFieldValidator {

    private static final Log LOGGER = LogFactory.getLog(PatientFieldValidator.class);

    public static void validateInsertOrUpdate(PatientDTO patientDTO) {
        List<String> errors = new ArrayList<>();
        if (patientDTO == null) {
            errors.add("patientDTO is null");
            throw new IncorrectParameterException(PatientDTO.class.getSimpleName(), errors);
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(PatientFieldValidator.class.getSimpleName(), errors);
        }
    }
}
