package com.myapplication.app;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.json.simple.JSONObject;

import java.io.IOException;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.concurrent.TimeoutException;

public class QueueConnection  {

    private String queue_name;
    private String host;

    QueueConnection(String queue_name, String host){
        this.queue_name=queue_name;
        this.host=host;
    }

    void sendMessage(JSONObject message) throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(this.host);

        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(this.queue_name, false, false, false, null);

        channel.basicPublish("", this.queue_name, null, message.toJSONString().getBytes());
        System.out.println(" Sent : '" + message + "'");

        channel.close();
        connection.close();
    }

    void insertActivity(Activity myActivity){
        String url;
        java.sql.Connection myConn = null;
        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver ());
            //Class.forName("com.mysql.jdbc.Driver");
            myConn=java.sql.DriverManager.getConnection("jdbc:mysql://mysqldb:3306/mydb","root","starwars");
            Statement myStm=myConn.createStatement();
            ResultSet myRes =myStm.executeQuery("select * from activity");
            String sql="insert into activity "+" (id,activity_name, start_date, end_date, activitypatient_id)"+"values ("+myActivity.getID()+","+" '"+myActivity.getActivity()+"',"+"'"+myActivity.getStart()+"', "+"'"+myActivity.getEnd()+"', "+"'"+1+"')";
            myStm.executeUpdate(sql);
            myConn.close();

        }
        catch (Exception e) {
            System.out.println(e.toString());
        }
    }
}
