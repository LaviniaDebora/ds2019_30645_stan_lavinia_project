
Pentru a crea/pornii un container se va executa comanda:
### docker-compose up

Se creaza un network cu ajutor comenzii:
###docker network create mynetwork

Containerul pentru baza de date:
###docker container run --name mysqldb --network mynetwork -e MYSQL_ROOT_PASSWORD=starwars -e MYSQL_DATABASE=mydb -d mysql:8.0.17

Vizualizare containere:
###docker container ls

Creare container pentru aplicatia spring boot si legarea in network:
###mvn clean
###mvn install
###docker image build -t spring .
###container run --network mynetwork --name spring-container-10 -p 8080:8080 -d spring

Creare container pentru fronted si legarea in network:
###docker image build -t react .
###container run --network mynetwork --name react-container-10 -p 3000:3000 -d react

Creare container pentru producer si legarea in network:
###docker image build -t messaging-app .
###container run --network mynetwork --name my-producer-10 -p 8081:8080 -d messaging-app

